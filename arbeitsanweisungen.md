#Arbeitsanweisungen
![Menü](screenshots/Arbeitsanweisungen/Menü.PNG)

Das Modul für interaktive Arbeitsanweisungen ermöglicht es Schritt-für-Schritt Anweisungen zu konfigurieren, auszuführen und anschließend auszuwerten.

##Konfiguration
![Konfiguration](screenshots/Arbeitsanweisungen/Konfiguration.PNG)

Bevor eine Arbeitsanweisung ausgeführt werden kann, muss diese erstellt werden. Dies geschieht über die Konfiguration. Wir empfehlen das die Konfiguration einer Arbeitsanweisung von einem Experten in der Ausführung vorgenommen wird. Nachträgliche Änderungen und Anpassungen einer Arbeitsanweisung sind jederzeit möglich. Zudem gibt es eine Versionierung, welche bei größeren Änderungen sinnvoll sein kann. Dabei ist zu beachten, dass sich nur die aktuellste Version bearbeiten lässt, während sich die älteren Versionen weiterhin nur anschauen lassen.

![Konfiguration Liste](screenshots/Arbeitsanweisungen/Konfiguration_Liste.PNG)

###Grundinformationen

![Grundinformationen](screenshots/Arbeitsanweisungen/Grundinformationen.PNG)

In der ersten hellblauen Box, werden die Grundinformationen definiert. Die aktuelle Versionsnummer befindet sich oben rechts.
1. Titel

    Anhand eines klaren Titels lässt sich die Arbeitsanweisung später besser wiederfinden.
2. Identifikation

    Falls die Arbeitsanweisung mit beispielsweise einem analogen Projekt oder einem Arbeitsauftrag identifiziert werden soll, lässt sich dort manuell ein Wert eingeben.
3. Beschreibung

    Eine grundliegende Beschreibung über die Arbeitsanweisung, lässt sich mit einem Text-Editor beschreiben. Der Editor ermöglicht verschiedene Text-Größen, **stärken**, *kursiv*, unterstrichen, Zitate, Listen, Aufzählungen und Einrückungen.
4. Anforderungen

    In dem Eingabefeld lässt sich definieren was benötigt wird um die Arbeitsanweisung auszuführen. Zusätzlich zu der Bezeichnung, lässt sich auch eine Anzahl und eigene Identifikationsnummer hinzufügen. Nach der Eingabe lässt es sich zu der sortierbaren Liste über den Hinzufügen-Button hinzufügen. Die Einträge lassen sich löschen, editieren und umsortieren.
5. Schwierigkeitsgrad

    Über den Slider lässt sich ein Schwierigkeitsgrad von 1 (sehr leicht) bis 10 (sehr schwer) festlegen.
6. Zeitaufwand

    Als letztes darf die geschätzte Dauer für das Ausführen aller Arbeitsschritte angegeben werden.

###Arbeitsschritte

![Arbeitsschritte](screenshots/Arbeitsanweisungen/Arbeitsschritte.PNG)

Der wohl wichtigste Teil einer Arbeitsanweisung sind detailierte Arbeitsschritte. Es lassen sich beliebig viele Schritte als Text- oder Bildanweisung hinzufügen. 
Arbeitsschritte lassen sich wie folgt konfigurieren:
- Jeder Schritt verfügt über den selben Text-Editor, damit eine genaue Beschreibung möglich ist.
- Der Titel und der Stil lässt sich editieren. Bei dem Stil lässt sich zwischen eine Farbgebung für Arbeitsanweisungen vom Typ Standard, Information, Gefahr, Erfolg oder Warnung wählen.
- In den Einstellungen lässt sich für jeden Schritt individuell festlegen ob dieser mit einem Bild oder mit einem Wert in einer definierbaren Einheit dokumentiert werden kann.
- Arbeitsschritte lassen sich per Drag-and-Drop umsortieren.
- Für eine bessere Übersicht, gibt es die Möglichkeit Schritte zu minimieren.
- Das Löschen eines Arbeitsschrittes ist ebenso möglich.

###Einstellungen

![Einstellungen](screenshots/Arbeitsanweisungen/Einstellungen.PNG)

Zu jeder Arbeitsanweisung lassen sich verschiedene Einstellungsmöglichkeiten setzen:
1. Alle **Anforderungen** müssen vor Beginn bestätigt werden.
2. Nach Bearbeitung dieser Arbeitsanweisung darf ein **Kommentar** hinterlassen werden.
3. Nach Bearbeitung dieser Arbeitsanweisung darf eine **Bewertung** hinterlassen werden.
4. Jeder Arbeitsschritt muss **bestätigt** oder mit einer **Begründung abgelehnt** werden.
5. Es wird ein bestimmtes **Resultat** erwartet.

##Ausführung

![Ausführung](screenshots/Arbeitsanweisungen/Ausführung.PNG)

Nach der erfolgreichen Konfiguration einer Arbeitsanweisung lässt sich diese beliebig oft ausführen.
Zu Beginn werden die Grundinformationen angezeigt und nach dem Bestätigen aller Anforderungen (kann sich je nach Konfiguration der Arbeitsanweisung unterscheiden), lässt sich der Start-Button betätigen. Anschließend folgen Schritt-für-Schritt die einzelnen Arbeitsschritte. Je nach Einstellung müssen diese bestätigt werden oder können mit einem Kommentar übersprungen werden. Zusätzlich können Bild- oder Wertdokumentationen erlaubt sein.
###Zusammenfassung

![Zusammenfassung](screenshots/Arbeitsanweisungen/Zusammenfassung.PNG)

Nach dem der letzte Schritt erreicht wurde, folgt die Zusammenfassung der Arbeit. Alle Schritte werden grob aufgelistet und es ist ersichtlich ob diese bestätigt oder abgelehnt wurden. Zu dem ist ersichtlich wann mit der Arbeit begonnen und aufgehört wurde. 
Je nach Einstellung lässt sich hier eine Bewertung und/oder Kommentar zu der Arbeitsanweisung abgeben. Außerdem kann ein vordefiniertes Resultat als Wert oder Bild erwartet werden.

##Auswertung
![Auswertung](screenshots/Arbeitsanweisungen/Auswertung_Menü.PNG)

Nach der erfolgreichen Ausführung wird das Ergebnis mit allen Eingaben und Informationen in der Auswertung gespeichert. Eine übersichtliche Auflistung aller ausgeführten Arbeitsanweisungen ist hier zu finden. Die komplette Ausführung lässt sich einsehen und abschließend als abgenommen/abgelehnt kennzeichnen.

![Auswertung](screenshots/Arbeitsanweisungen/Auswertung_Liste.PNG)