#Aufgaben
##Ressourcenkalender
Der Ressourcenkalender verbindet die Aufgabenverwaltung mit dem Personalmanagement. 

###Personalspalten
Jedes Personal hat eine eigene Spalte und ist nach Tätigkeit sortiert.
Die zeitliche Zuteilung der Aufgaben in der Personalspalte erfolgt über ein gewöhnliches Drag-and-Drop prinzip. Aufgaben lassen sich mit der Maus zeitlich und personalübergreifend verschieben und anpassen.
Per Rechtsklick auf eine Aufgabe in der Personalspalte lässt sich die Aufgabe löschen.

###Aufgabenliste
- Die Aufgaben sind standardmäßig nach Dringlichkeit sortiert. Die Sortierung nach Standort ist möglich. Alle Standorte haben eine eigene Farbgebung, welche durch den Namen generiert wird.
- In der Suche lässt sich unter anderem nach der Bezeichnung oder der Beschreibung der Aufgabe suchen.
- Per Klick auf eine Aufgabe werden weitere Informationen in der Infobox angezeigt.
- Per Doppelklick auf eine Aufgabe wird in die Konfiguration gewechselt.
- Aufgaben lassen sich per Drag-and-Drop auf die entsprechende Personalspalte zeitlich zuordnen.

###Generell
- Warnungen bei der Personalüberlastung erfolgen. (Wochenansicht standardmäßig 40 Stunden, Tagesansicht standardmäßig 8 Stunden)
- Typische Arbeitszeiten sind hell hinterlegt, während untypische grau hinterlegt sind.
- Beim Zuteilen einer Aufgabe in die Vergangenheit wird geprüft ob dies tatsächlich gewollt ist.
- Bereits zugeteilte Aufgaben lassen sich erneut zuteilen und sind im Reiter 'Zugeteilte Aufgaben' zu finden.
- Der offene Zeitaufwand von nicht zugeteilten Aufgaben lässt sich anzeigen.
- Feiertage werden dargestellt und Urlaubseintragungen sind möglich.

###Kalender Exportierung
Exportieren Sie Ihren persönlichen Ding.X-Kalender mit diesen einfachen Schritten in Ihr bevorzugtes Kalender-Programm.

Um den Personalkalender zu erhalten, navigieren Sie in das ***Personal***.
Wählen Sie das Kalender-Symbol des entsprechenden Personals aus, um die URL in die Zwischenablage zu kopieren. Zur gleichen Zeit wird versucht den Link über Ihr standard Kalender-Programm zu öffnen.
![](screenshots/personal-kalenderlink.jpg)

Nachfolgend wird umschrieben wie einige gängige Kalenderprogramme diese URL abonnieren können.

####Google Kalender
Wichtiger Hinweis zu Ding.X und Google: Wenn Änderungen an Ihren Zeitplänen in Ding.X vorgenommen werden, werden diese Änderungen sofort in die .ics-Kalenderdatei in unserem System geschrieben und stehen somit sofort über die URL zur Verfügung. Bei diesem Vorgang handelt es sich jedoch seitens Google um einen reinen Pull-Vorgang, bei dem Google Kalender Ding.X aktiv kontaktieren muss, um die neueste Version des Kalenders unter der angegebenen URL abzurufen. Google Kalender neigt dazu diese Aktion selten und in scheinbar zufälligen Intervallen auszuführen. Die Aktualisierungsrate kann von Google-Nutzern nicht konfiguriert (oder eingesehen) werden. Aus diesem Grund werden Änderungen an Ihren Zeitplänen in Ding.X möglicherweise nicht innerhalb von 20 bis 30 Stunden in Google angezeigt. Wenn Sie eine Aktualisierung erzwingen müssen, lesen Sie den unten beschriebenen Vorgang zum Erzwingen der Regenerierung.

Über der Liste Ihrer Kalender im linkem Menü, klicken Sie auf das "+"-Symbol neben *Weitere Kalender hinzufügen* und wählen *Per URL*.

![](screenshots/google-addCalendar.jpg)

Fügen Sie die URL des Personalkalenders, welchen Sie aus Ding.X kopiert haben, ein und wählen anschließend *Kalender hinzufügen*.

![](screenshots/google-perURL.jpg)

Nun sollte der Kalender unter dem *Weitere Kalender* Bereich im Menü auf der linken Seite auftauchen.

###Google zur Aktualisierung zwingen
Wie oben erläutert, erlaubt Google den Benutzern nicht die Aktualisierungsrate für importierte Kalender zu steuern. Daher ist es möglicherweise erforderlich eine Aktualisierung mit der folgenden Problemumgehung zu erzwingen, wenn Sie sofortige Änderungen sehen müssen.

Suchen Sie im Google Kalender nach dem von Ihnen abonnierten Ding.X Ressourcenkalender in dem Menü auf der linken Seite, fahren Sie mit der Maus über den Eintrag und wählen Sie das "x"-Symbol um den Kalender zu entfernen.

![](screenshots/google-unsub.jpg)

Folgen Sie nun den obigen Anweisungen um einen Kalender per URL hinzuzufügen. Nur dieses Mal fügen Sie die folgende Abfragezeichenfolge an das Ende der von Ding.X bereitgestellten .ics-Kalender URL an.

**?nocache**

Die resultierende URL sollte wie in diesem Beispiel aussehen:

**webcal://portal2.north-tec.de/calendars/xxxxxxxxxxxxx.ics?nocache**

Dies führt dazu, dass Google diese URL kontaktiert und die neueste Version der .ics-Datei einschließlich der letzten Änderung lädt, anstatt die zwischengespeicherte Version zu laden. Es ist jedoch wichtig anzumerken, dass dies nur eine einmalige Aktualisierung erzwingt, nach der die neue URL (einschließlich der nocache-Anweisung) nun zwischengespeichert wird. Um diese Operation ein zweites Mal durchzuführen, müssen Sie die nocache-Anweisung jedes Mal um eine Zahl erhöhen (?nocache1, ?nocache2, ?nocache3 usw.).

####Outlook
Outlook lässt sich im Browser als Webanwendung sowie als reguläres Programm auf Ihrem Rechner ausführen.
#####Web
1. In der Navigation von Outlook, klicken Sie auf App Launcher und anschließend auf den **Kalender**.
![](screenshots/outlookweb-selectCal.jpg)

2. Unter der Outlook-Kalender Navigation, wählen Sie **Kalender hinzufügen** und anschließend **Aus dem Internet**.
![](screenshots/outlookweb-selectURL.jpg)

3. Nun öffnet sich an der rechten Seite die Einstellungen für das Kalenderabonnement. Dort tragen lässt sich im Feld **Link zum Kalender** die URL des Ding.X Ressourcenkalenders eintragen. Es empfiehlt sich zudem einen vernünftigen Kalendernamen zu vergeben, damit Sie den Kalender später gut von Ihren Anderen unterscheiden können.
![](screenshots/outlookweb-aboURL.jpg)

4. Abschließend drücken Sie auf **Speichern**.

#####Windows
####iCal
1. Wählen Sie **Ablage** > **Neues Kalenderabonnement**.
![](screenshots/ical-selectURL.jpg)

2. Geben Sie die URL des Kalenders ein und klicken Sie auf **Abonnieren**.
![](screenshots/ical-aboURL.jpg)

3. Nun befinden Sie sich in den Einstellungen des Kalenders. Geben Sie dort einen Namen für den Kalender in das Feld **Name** ein und klicken Sie auf das angrenzende Popup-Menü um eine Farbe auszuwählen. 

4. Klicken Sie auf das Popup-Menü **Ort** und wählen Sie ein Konto für das Abonnement aus. Wenn Sie Ihr iCloud-Konto auswählen, ist der Kalender auf allen Ihren Computern und Geräten verfügbar, die mit iCloud eingerichtet wurden. Wenn Sie "Lokal" auswählen, wird der Kalender nur auf Ihrem Computer gespeichert.

5. Um die Ereignisanhänge oder Hinweise des Kalenders zu erhalten, deaktivieren Sie die entsprechenden Kontrollkästchen. (Diese Funktionen werden noch nicht von Ding.X unterstützt.)

6. Klicken Sie auf das Popup-Menü **Automatisch aktualisieren**, und wählen Sie dann aus, wie oft der Kalender aktualisiert werden soll. Eine sehr kurze Aktualisierungszeit ist hier empfehlenswert, damit Änderungen aus Ihrem Ressourcenkalender in Ding.X schnellstmöglich in Ihrem Apple Kalender erscheinen.

7. Um zu verhindern, dass Hinweise für diesen Kalender angezeigt werden, wählen Sie **Hinweise ignorieren** aus.

8. Klicken Sie auf **OK**.
![](screenshots/ical-cfgURL.jpg)

Um später Änderungen vorzunehmen, rechtsklicken Sie auf den Namen des Kalenders und wählen Sie **Informationen** um in die Konfiguration des Abonnements zu gelangen. 

##Übersicht
###Standorte