#Werkzeuge
##Gesammelt
Alle Betriebsmittelklassen werden hier aufgelistet.
###Suche
In dem Suchfeld lässt sich nach der Bezeichnung der Klasse suchen.
###Hinzufügen
Um eine neue Betriebsmittelklassen hinzuzufügen, gibt es zwei Möglichkeiten:
1. Durch das Klicken auf den ![](screenshots/bmk-add-button.png)-Button, öffnet sich die [Konfiguration](#konfiguration) für eine komplett neue Betriebsmittelklasse.
2. Um sich die erste Möglichkeit zu ersparen, lassen sich ausgewählte, bereits konfigurierte Betriebsmittelklassen [importieren](#importieren).

###Bearbeiten
Durch den Klick auf das ![](screenshots/bmk-edit.png)-Symbol, wird die Konfiguration der Betriebsmittelklasse erneut geöffnet und die Felder lassen sich anpassen.
###Klonen
Eine Kopie der Betriebsmittelklasse lässt sich über den ![](screenshots/bmk-clone.png)-Button anlegen. Das ist zum Beispiel praktisch, wenn eine leicht abgeänderte Version einer Betriebsmittelklasse erstellt werden soll.
###Löschen
Über den ![](screenshots/bmk-delete.png)-Button werden Betriebsmittelklassen komplett aus dem System entfernt.
###Importieren
Über das Importieren-Fenster, lassen sich globale Betriebsmittelklassen in die eigenen übernehmen. Die globalen Betriebsmittelklassen sind von *North-Tec* freigegeben und stellen häufig gebrauchte Klassen dar. Nach dem initialem Importieren, lässt sich die Klasse direkt in den Betriebsmitteln verwenden. Sobald *North-Tec* Änderungen an der Klasse vornimmt, werden diese auch in der importieren Klasse übernommen. Falls eigene Änderungen an der Klasse vorgenommen werden sollen, lässt sich diese Klonen und anschließend Bearbeiten. Dadurch werden aber keine weiteren Änderungen an der geklonten Klasse durch *North-Tec* übernommen. Importiere Klassen lassen sich durch das Abwählen und das anschließende Speichern in der Importauswahl entfernen.
Importierte Klassen sind mit einem Wolken-Symbol gekennzeichnet.

![](screenshots/bmk-import.png)

___
##Konfiguration
Im Konfigurator einer Betriebsmittelklassen, lassen sich verschiedene Eigenschaften festlegen. Diese sind in folgende drei Bereiche unterteilt:
1. **allgemeine Daten:** Für Beispielsweise die Seriennummer oder den Hersteller des Betriebsmittels.
2. **physikalische Daten:** Relevante Daten für die physikalischen Eigenschaften. Beispielsweise die Länge oder das Volumen.
3. **kalkulatorische Daten:** Daten die für die Berechnung des Verbrauches oder Ertrages des Betriebsmittels relevant sind. Bei einem Fahrzeug könnte es sich um die Anzahl verbrauchter Liter Benzin pro 100km handeln.

In jedem dieser Bereiche lassen sich beliebig viele Eigenschaften hinzufügen, sortieren, bearbeiten und auch wieder löschen. Eine Eigenschaft verfügt über einen Standardwert sowie einer Einheit/Suffix.
Grundliegend lässt sich der Betriebsmittelklasse eine Bezeichnung, eine Kategorie und Tags zuweisen. Außerdem lässt sich eine Klasse als Ersatzteilrolle für eine andere Klasse deklarieren.
Nach dem erfolgreichem Anlegen, lässt sich aus der Klasse ein neues [Betriebsmittel](betriebsmittel.md) erstellen.
___
##Kategorien
Jede Betriebsmittelklasse kann über den Konfigurator einer Kategorie zugeordnet werden. In der Kategorienübersicht sind alle der Betriebsmittelklassen zugeordneten Kategorien gesammelt und die Anzahl der Betriebsmittelklassen in der jeweiligen Kategorie wird ersichtlich. Durch das öffnen einer Kategorie werden nur Betriebsmittelklassen aus dieser Kategorie dargestellt.
Wenn eine Betriebsmittelklasse einer Kategorie zugeordnet wird, welche noch nicht vorhanden ist, dann wird diese in der Kategorienübersicht zunächst ohne Farbe und Icon angezeigt. Das Anpassen ist anschließend über das Bearbeiten möglich.

![](screenshots/bmk-category-overview.png)

###Bearbeiten
Kategorien lassen sich über den ![](screenshots/bmk-edit.png)-Button bearbeiten. Der Name, die Farbe und das Icon lassen sich dort anpassen. Bei den Farben handelt es sich um eine vorselektierte Auswahl und bei den Icons lässt sich aus den Icons der [Font Awesome](http://fontawesome.io) wählen.

![](screenshots/bmk-category-edit.png)

___
##Hierarchie
Betriebsmittelklassen lassen sich hierarchisch anlegen. Dafür wird in eine Betriebsmittelklasse mit einem Klick auf die Bezeichnung navigiert und alle untergeordneten Klassen werden dargestellt. Dies lässt sich beliebig weit vertiefen und an jeder Position in der Hierarchie lässt sich eine neue Betriebsmittelklasse hinzufügen.
Importierte Betriebsmittelklassen können auch über untergeordnete Klassen verfügen. 