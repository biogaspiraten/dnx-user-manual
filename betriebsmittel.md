#Betriebsmittel
 Betriebsmittel stellen die Grundlage im gesammten System innhalb von Ding.X.
 Sie sind der Digitale Zwilling an dem man weitere Informationen, wie Dokumente, Messwerte, Berichte etc. anheften kann.
 

 ### Standorte / Gruppierungen

Jedes Betriebsmittel beginnt mit einer Gruppierung die nach Vorangig nach Standorten eingeteilt werden können. Die Einteilung befinden sich innerhalb des Seitenmenus. 

![Seitenmenu](screenshots/Betriebsmittelmanagement/seitenmenu.PNG)


Über ![!Hinzufügen](screenshots/Betriebsmittelmanagement/hinzufügen.PNG) können neu Gruppierung/Standorte hinzugefügt werden.

![Standorteingabe](/screenshots/Betriebsmittelmanagement/standorteingabe.PNG "Eingabeformular zum Standort")

###Betriebsmittelübersicht
Alle Betriebsmittel befinden sich unterhalb eines Standortes. Die Information werden gegliedert in der Listenansicht, Dashboard und der Zeitleiste.
![Betriebsmittelübersicht](/screenshots/Betriebsmittelmanagement/betriebsmittelübersicht.PNG) 

####Listenansicht
Die Listenansicht führt alle Betriebsmittel auf, die sich durch hierachische Gliederung unterteilen. Ein Betriebsmittel kann weitere Betriebsmittel enthalten die sich darunter Gliedern. Über das Infosymbol ![Info](screenshots/Betriebsmittelmanagement/info.PNG) werden die Informationen zu dem Betriebsmittel im darunterliegenden Dashboard angezeigt. 

Über ![Link](screenshots/Betriebsmittelmanagement/link.PNG) ist es möglich das Betriebsmittel mit Dokumenten und Aufgaben zu verlinken.
![Verlinken](screenshots/Betriebsmittelmanagement/verlinken.PNG)
Die Menupunkte Aufgaben, Berichte und Betriebsmittelklasse sich noch in der Entwicklung.

![edit](screenshots/Betriebsmittelmanagement/edit.PNG) ist für die Bearbeitung des Betriebsmittels.

Mit ![delete](screenshots/Betriebsmittelmanagement/delete.PNG) wird das Betriebsmittel gelöscht.

Über die **Suche** können Betriebsmittel standortübergreifend gefunden werden.

####Betriebsmittel-Dashboard

![Dashboard](screenshots/Betriebsmittelmanagement/dashboardextend.PNG)
Das Betriebsmittel-Dashboard zeigt ein Übersicht über das momentan gewählte Betriebsmittel.

####Zeitleiste

Durch Erfassungen von Standorten, Schadensaufnahmen, erledigten Aufgaben und Notizen entsteht eine Lebensakte des Betriebsmittels. Diese ist in Form einer Zeitleiste dargestellt. Dort ist ersichtlich zu welchem Zeitpunkt und meistens auch von welcher Person Informationen bezüglich des ausgewählten Betriebsmittels erfasst worden.
Die Zeitleiste lässt sich nach den verschiedenen Typen der Eintragungen Filtern und bietet bis hin zur Initialisierung eine Übersicht über den kompletten zeitlichen Ablauf.
Notizen bzw. freie Eingaben lassen sich auf der Zeitleiste platzieren.
##Standorte